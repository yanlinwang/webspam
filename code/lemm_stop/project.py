import math
import os
import re

words_min_size = 4
def istxt(file):
    return file.endswith(".txt")
def get_words_from_string(s):
    return list(re.findall(re.compile('[a-zA-Z]+'), s.lower()))
def get_words_from_file(fname):
    with open(fname, 'rb') as inf:
        return get_words_from_string(inf.read())
def get_words_from_string_sub(s):
    return list(re.findall(re.compile('[a-zA-Z]+'), s.splitlines()[0].lower()))
def get_words_from_file_sub(fname):
    with open(fname, 'rb') as inf:
        return get_words_from_string_sub(inf.read())
def get_words_from_string_con(s):
    return list(re.findall(re.compile('[a-zA-Z]+'), s.split('\n', 1)[-1].lower()))
def get_words_from_file_con(fname):
    with open(fname, 'rb') as inf:
        return get_words_from_string_con(inf.read())

words_to_ignore = ["that", "subject", "what","with","this","would","from",
                   "your","which","while","these", "have", "here", "they",
                   "will"]

train_dir = './part1'
train_spam = [name for name in os.listdir(train_dir) if name.startswith("spm")]
train_ham = [name for name in os.listdir(train_dir) if not name.startswith("spm")]
train_spam_num = len(train_spam)
train_ham_num = len(train_ham)
test_dir = './part2'
test_spam = [name for name in os.listdir(test_dir) if name.startswith("spm")]
test_ham = [name for name in os.listdir(test_dir) if not name.startswith("spm")]
test_spam_num = len(test_spam)
test_ham_num = len(test_ham)

print 'Train spam num: ', train_spam_num
print 'Train ham  num: ', train_ham_num
print 'Test spam  num: ', test_spam_num
print 'Test ham   num: ', test_ham_num

def smooth(spam_words_sub, ham_words_sub, words_count_sub):
    spam_words_total_sub = len(spam_words_sub)
    ham_words_total_sub = len(ham_words_sub)
    for word in words_count_sub:
        if words_count_sub[word][0] == 0:
            # Laplace smoothing
            words_count_sub[word][0] = math.log(1 / (float(spam_words_total_sub) + 2))
        else:
            words_count_sub[word][0] = math.log(words_count_sub[word][0] / float(spam_words_total_sub))

        if words_count_sub[word][1] == 0:
            # Laplace smoothing
            words_count_sub[word][1] = math.log(1 / (float(ham_words_total_sub) + 2))
        else:
            words_count_sub[word][1] = math.log(words_count_sub[word][1] / float(ham_words_total_sub))
    return words_count_sub

def get_all_sub(path, listing):
    result = []
    for infile in listing:
        if istxt(infile):
            result += get_words_from_file_sub(path + '/' + infile)
    return result
def get_all_con(path, listing):
    result = []
    for infile in listing:
        if istxt(infile):
            result += get_words_from_file_con(path + '/' + infile)
    return result

###### subject ######
##spam
words_count_sub = {}
spam_words_sub = get_all_sub(train_dir, train_spam)
for word in spam_words_sub:
    if word not in words_to_ignore and len(word) >= words_min_size:
        if word in words_count_sub:
            words_count_sub[word][0] += 1
        else:
            words_count_sub[word] = [1, 0]
##ham
ham_words_sub = get_all_sub(train_dir, train_ham)
for word in ham_words_sub:
    if word not in words_to_ignore and len(word) >= words_min_size:
        if word in words_count_sub:
            words_count_sub[word][1] += 1
        else:
            words_count_sub[word]= [0, 1]

## calculate prob
smooth(spam_words_sub, ham_words_sub, words_count_sub)

## testing begins
train_spam_pro = math.log(train_spam_num / float(train_spam_num + train_ham_num))
train_ham_pro = math.log(train_ham_num / float(train_spam_num + train_ham_num))

# testing ham emails in testing set
predicted_ham_num = 0
predicted_spam_num = 0
bingo_ham_num = 0
bingo_spam_num = 0

for infile in test_ham:
    test_words = get_words_from_file_sub(test_dir + '/' + infile)
    spam_pro = train_spam_pro
    ham_pro = train_ham_pro
    for word in test_words:
        if word in words_count_sub:
            spam_pro += words_count_sub[word][0]
            ham_pro += words_count_sub[word][1]
    if spam_pro < ham_pro:
        bingo_ham_num += 1
        predicted_ham_num += 1
    else:
        predicted_spam_num += 1

# testing spam emails in testing set
for infile in test_spam:
    test_words = get_words_from_file_sub(test_dir + '/' + infile)
    spam_pro = train_spam_pro
    ham_pro = train_ham_pro
    for word in test_words:
        if word in words_count_sub:
            spam_pro += words_count_sub[word][0]
            ham_pro += words_count_sub[word][1]
    if spam_pro < ham_pro: 
        predicted_ham_num += 1
    else:
        bingo_spam_num += 1
        predicted_spam_num += 1

print '(Sub)The accuracy for test ham data is', bingo_ham_num , float(predicted_ham_num), bingo_ham_num / float(predicted_ham_num)
print '(Sub)The accuracy for test spam data is', bingo_spam_num, float(predicted_spam_num), bingo_spam_num / float(predicted_spam_num)
print '(Sub)The recall for test ham data is', bingo_ham_num , float(test_ham_num), bingo_ham_num / float(test_ham_num)
print '(Sub)The recall for test spam data is', bingo_spam_num, float(test_spam_num), bingo_spam_num / float(test_spam_num)


###### content ######
##spam
words_count_con = {}
spam_words_con = get_all_con(train_dir, train_spam)
for word in spam_words_con:
    if word not in words_to_ignore and len(word) >= words_min_size:
        if word in words_count_con:
            words_count_con[word][0] += 1
        else:
            words_count_con[word] = [1, 0]
##ham
ham_words_con = get_all_con(train_dir, train_ham)
for word in ham_words_con:
    if word not in words_to_ignore and len(word) >= words_min_size:
        if word in words_count_con:
            words_count_con[word][1] += 1
        else:
            words_count_con[word]= [0, 1]

## calculate prob
words_count_con = smooth(spam_words_con, ham_words_con, words_count_con)

## testing begins
train_spam_pro = math.log(train_spam_num / float(train_spam_num + train_ham_num))
train_ham_pro = math.log(train_ham_num / float(train_spam_num + train_ham_num))

# testing ham emails in testing set
predicted_ham_num = 0
predicted_spam_num = 0
bingo_ham_num = 0
bingo_spam_num = 0

for infile in test_ham:
    test_words = get_words_from_file_con(test_dir + '/' + infile)
    spam_pro = train_spam_pro
    ham_pro = train_ham_pro
    for word in test_words:
        if word in words_count_con:
            spam_pro += words_count_con[word][0]
            ham_pro += words_count_con[word][1]
    if spam_pro < ham_pro:
        bingo_ham_num += 1
        predicted_ham_num += 1
    else:
        predicted_spam_num += 1

# testing spam emails in testing set
for infile in test_spam:
    test_words = get_words_from_file_con(test_dir + '/' + infile)
    spam_pro = train_spam_pro
    ham_pro = train_ham_pro
    for word in test_words:
        if word in words_count_con:
            spam_pro += words_count_con[word][0]
            ham_pro += words_count_con[word][1]
    if spam_pro < ham_pro: 
        predicted_ham_num += 1
    else:
        bingo_spam_num += 1
        predicted_spam_num += 1

print '(Con)The accuracy for test ham data is', bingo_ham_num , float(predicted_ham_num), bingo_ham_num / float(predicted_ham_num)
print '(Con)The accuracy for test spam data is', bingo_spam_num, float(predicted_spam_num), bingo_spam_num / float(predicted_spam_num)
print '(Con)The recall for test ham data is', bingo_ham_num , float(test_ham_num), bingo_ham_num / float(test_ham_num)
print '(Con)The recall for test spam data is', bingo_spam_num, float(test_spam_num), bingo_spam_num / float(test_spam_num)

print (bingo_spam_num+bingo_ham_num)/float(test_ham_num + test_spam_num)