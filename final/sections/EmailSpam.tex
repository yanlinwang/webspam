\section{Case Study - Email Spam}
Spam email is any email that was not requested by a user but was sent to that
user and many others, typically (but not always) with malicious intent. The
source and identity of the sender is anonymous and there is no option to cease
receiving future emails. Although most of us can easily identify a spam message
and ignore it, there are still individuals in the world who will respond to spam
messages, sending malicious spammers sensitive financial or personal
information. From the spammer's point of view, the cost in money and time to
send spam emails is quite low. The more spam emails a spammer sends, the more
likely he or she is to get recipients to respond to the email. A recent study
says that more than 70\% of the total messages that are sent over the internet
are spam \cite{androutsopoulos2000evaluation}. Hence the development of an
effecive and efficient spam filter is highly imperative. The classical naive
bayes approach was used to develop the spam filter. Naive Bayes classifier is a
probabilistic classifier based on Bayes' theorem. The theorem assumes that each
feature is conditionally indepent of each other. The Ling-Spam dataset was used
to train and test our classifier \cite{androutsopoulos2000evaluation}. There are
four subdirectories, corresponding to four versions of the corpus. Each one of
these 4 directories contains 10 subdirectories, and each one of the 10
subdirectories contains both spam and legitimate messages, one message in each
file. In this paper, we use the lemmatiser enabled and stop-list enabled
directory. Lemmatisation is the process of grouping together the different forms
of a word so they can be analysed as a single item. For example, the verb `walk'
may appear as `walk', `walked', `walks', `walking'. The base form of all these
words is `walk'. Stop words usually refer to the most common words in a language
such as `the' or `that', which can be filter out before processing the email
texts. In our experiments, each email message is ultimately represented as a
vector $<x_1,x_2,...,x_m>$. From Bayes' theorem, the probability that a message
with $\vec{x}=<x_1,x_2,...,x_m>$ belongs in category $c$ is:
\begin{equation}
P(c|\vec{x})=\frac{P(c)P(\vec{x}|c)}{P(\vec{x})}
\end{equation}

Naive Bayes classifies each message in the category that maximizes
$P(c)P(\vec{x}|c)$. In the case of spam filtering, this is equivalent to
classifying a message as spam whenever:

\begin{equation}
\frac{P(c_{spam})P(\vec{x}|c_{spam})}{P(c_{ham})P(\vec{x}|c_{ham})+P(c_{spam})P(\vec{x}|c_{spam})}>T
\end{equation}
with $T=0.5$, where $c_{spam}$ and $c_{ham}$ denote the spam and ham categories.

\subsection{Cost-sensitive Evaluation}
The classification model is usually evaluated on accuracy and error rate. Since
the cost of classifying a legitimate message as spam (false positive) far
outweighs the cost of classifying spam as legitimate (false negative), we should
take this cost sensitivity into consideration and reduce the false positive
rate. For the cost-sensitive evaluations, we assume that misclassifying a
legitimate email as spam is $\lambda$ times more costly than misclassifying a
spam email as legitimate. If a legitimate message is misclassified, it counts as
$\lambda$ errors, and if it is classified correctly, it counts as $\lambda$
successes. This assumption can be formulated in the form of a weighted accuracy
\cite{androutsopoulos2000experimental}:
\begin{equation}
weighted \ accuracy=\frac{\lambda(n_{h \rightarrow h}+n_{s \rightarrow s})}{\lambda(N_h+N_s)}
\end{equation}
where where $n_{h \rightarrow h}$ denotes the number of emails classified as
legitimate which truly are, similarly $n_{s \rightarrow s}$ denotes the number
of spam emails classified as spam, $N_h$ and $N_s$ are the number of legitimate
and spam emails to be classified by the spam filter.

\subsection{Alternative Approach: Co-training}
Co-training is a machine learning algorithm used when there are only small amounts of labeled data and large amounts of unlabeled data. It is applicable to email spam detection. In practical scenarios, we can only obtain a small training data set of labeled spam and ham emails while large amount of emails remains unlabeled. The process for co-training can be summarized as follows:\\
\rule{0.48\textwidth}{0.2mm}\\
\textbf{Step 1:} It assumes that each example is described using two different feature sets (views). Ideally, the two views are conditionally independent (i.e., the two feature sets of each instance are conditionally independent given the class) and each view is sufficient.\\
\textbf{Step 2:} Co-training first learns a separate classifier for each view using any labeled examples.\\
\textbf{Step 3:} The most confident predictions of each classifier on the unlabeled data are then used to iteratively construct additional labeled training data.\\
\rule{0.48\textwidth}{0.2mm}

\subsection{Experiment}
\subsubsection{Dataset}
We do experiments on the following datasets:
\begin{enumerate}
\item{Ling-Spam dataset (A):}

  There are four subdirectories, corresponding to four versions of the corpus:

  bare: Lemmatiser disabled, stop-list disabled.

  lemm: Lemmatiser enabled, stop-list disabled.

  lemm-stop: Lemmatiser enabled, stop-list enabled.

  stop: Lemmatiser disabled, stop-list enabled.

  Each one of these 4 directories contains 10 subdirectories (part1, ...,
  part10). These correspond to the 10 partitions of the corpus that were used in
  the 10-fold experiments. In each repetition, one part was reserved for testing
  and the other 9 were used for training.

  Each one of the 10 subdirectories contains both spam and legitimate messages,
  one message in each file. Files whose names have the form spmsg*.txt are spam
  messages. All other files are legitimate messages. All other files are
  legitimate messages.

  There are about 2900 emails in total.
\item{Another dataset (B):}

  About 17000 emails in total. Its statistics is shown in
  Table~\ref{table:dataset}. A sample ham is shown in Figure~\ref{fig:ham}.

\begin{table}[]
\centering
\caption{Dataset (B)}
\label{table:dataset}
\begin{tabular}{|l|l|l|}
\hline
\rowcolor[HTML]{68CBD0}
(B)                           & Spam & Ham  \\ \hline
\cellcolor[HTML]{CD9934}Train & 3366 & 9034 \\ \hline
\cellcolor[HTML]{CD9934}Test  & 1124 & 3011 \\ \hline
\end{tabular}
\end{table}

\end{enumerate}

\begin{figure*}
\centering
\includegraphics[scale=0.6]{ham.png}
\caption{A Sample Ham}
\label{fig:ham}
\end{figure*}

\subsection{Experimental Results}
\subsubsection{Naive Bayes}
The overall results (accuracy) on the two datasets only using naive bayes are:
\begin{itemize}
\item (A) 95\%
\item (B) 98.4\%
\end{itemize}

\subsubsection{Co-training}
The intermediate results on dataset (A) using co-training are:
\begin{itemize}
\item Subtitle: 89.3\%
\item Content: 97.7\%
\end{itemize}

Detailed result is shown in Figure~\ref{fig:co-train}, it is an intermediate
result. As shown in the figure, the accurary is already pretty high, so it
becomes hard to decide what samples can be put into training set.

\begin{figure*}
\centering
\includegraphics[scale=0.6]{result.png}
\caption{Co-training result}
\label{fig:co-train}
\end{figure*}

\begin{figure*}
\centering
\includegraphics[width=5in]{res3.png}
\caption{Twitter spammer detection result}\label{fig:twitter}
\end{figure*}


